import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Customer} from "./customer.modele";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  PHP_API_SERVER = "http://localhost:8888";

  constructor(private httpClient: HttpClient) { }

  getCustomers(): Observable<Customer[]>{
    return this.httpClient.get<Customer[]>(`${this.PHP_API_SERVER}/api/v1/customers`);
  }
}
