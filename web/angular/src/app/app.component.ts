import {Component, OnInit} from '@angular/core';
import {ApiService} from "./api.service";
import {Observable} from "rxjs";
import {Customer} from "./customer.modele";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: String = 'angular';
  private data: Observable<Customer[]>;
  private customer: Customer[];


  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.getCustomers().subscribe((customer: Customer[])=>{
      this.customer = customer;
      console.log(this.customer);
    })
  }

}
